var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var db_name = "mars_db";
var mongo_connection_string = 'mongodb://127.0.0.1:27017/' + db_name;

if(process.env.OPENSHIFT_MONGODB_DB_URL){
  mongodb_connection_string = process.env.OPENSHIFT_MONGODB_DB_URL + db_name;
}

var semSchema = mongoose.Schema({
  sem: {type: String, default: " "},
  section: {type: String, default: " "},
  sub: {type: String, default: " "}
});

var faculty = mongoose.Schema({
  name: String,
  id: String,
  password: String,
  semesters: [semSchema]
});

mongoose.connect(mongo_connection_string);
var Faculty = module.exports = mongoose.model('Faculty', faculty);
