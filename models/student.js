var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var db_name = "mars_db";
var mongo_connection_string = 'mongodb://127.0.0.1:27017/' + db_name;

if(process.env.OPENSHIFT_MONGODB_DB_URL){
  mongodb_connection_string = process.env.OPENSHIFT_MONGODB_DB_URL + db_name;
}

var attendanceSchema = mongoose.Schema({
  subject: {type: String},
  date: {type: String},
  status: {type: String}
});

var marksSchema = mongoose.Schema({
  subject: {type: String},
  examination: {type: String},
  marks: {type: String}
});

var student = mongoose.Schema({
  name: String,
  rollNumber: String,
  semester: String,
  section: String,
  marks: [marksSchema],
  attendance: [attendanceSchema]
});

var Student = module.exports = mongoose.model('Student', student);
