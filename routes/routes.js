var express = require('express');
var mongoose = require('mongoose');
var fs = require('fs');
var faculties = require('config/addFaculty');
var addStudent = require('config/addStudent');
Faculty = require("../models/faculty");
Student = require("../models/student");
Demo = require("../models/student.json");
Example = require("../models/faculty.json");
var faculty_login = require('config/faculty_login');
var gcm = require('node-gcm');
var sender = new gcm.Sender('AAAADDGIEY4:APA91bFdI0_ddLh-t72Fu00HVGpqJvQhQiHoGOnMb8hSxz9FveEeN91mQ7CIgyVfP0p7lxInskOw4jzT_ZNxnDqGzftKqClajMDkSZ_nvcPMTi8KC5Bilitwk4S4nELvhPImtAIyBen7OAgrxdDNND7ZfE06qb1EcQ');
var regTokens = ['dDDOIggTGt4:APA91bFQ-orsZdEOeLEglrXYhueD0wGNhKtutS5Cr7xy6YYfRCWV7V3ec2yf1OnO-65HYlwF4BDUQnn392ADHy2lnfkLzue7Y5hAIICgn0YLMHSA6_rbO6gYuoE-YQ_PHLCP5dMZ5pxm'];

module.exports = function(app){
  app.get('/', function(req, res){
    res.json({"response": "Mars went online now!!!"});
  });

  app.get('/faculty.html', function(req, res){
    res.sendfile(__dirname + "/" + "faculty.html");
  });

  app.get('/academicCalendar', function(req, res){
    fs.readFile(__dirname + "/" + "academic_calendar.pdf", function(err, data){
      res.contentType("application/pdf");
      console.log("Success");
      res.send(data);
      if (err) {
        console.log(err);
      }
    });
  });

  app.get('/csSyllabus', function(req, res){
    fs.readFile(__dirname + "/" + "cs_syllabus.pdf", function(err, data){
      res.contentType("application/pdf");
      console.log("Success");
      res.send(data);
      if (err) {
        console.log(err);
      }
    });
  });

  app.get('/student.html', function(req, res){
    res.sendfile(__dirname + "/" + "student.html");
  });

  app.post('/addFaculty', function(req, res){
    var name = req.body.facultyName;
    var id = req.body.id;
    var password = req.body.password;
    var sem = new Array();
    sem.push({
      "sem": req.body.sem,
      "section": req.body.sec,
      "sub": req.body.sub
    });
    faculties.addFaculty(name, id, password, sem, function(found){
      console.log(found);
      res.json(found);
    });
  });

  app.get('/faculty', function(req, res){
    Faculty.find(function(err, users){
      if (err) {
        res.send(err);
      }
      res.json(users);
    });
  });

  app.post('/faculty_login', function(req, res){
    var id = req.body.id;
    var password = req.body.password;
    faculty_login.login(id, password, function(found){
      console.log(found);
      res.json(found);
    });
  });

  app.post('/addStudent', function(req, res){
    if (req.body.students) {
      console.log(req.body.students);
      Student.create(req.body.students, function(err){
        if (err) {
          res.send(err);
        } else {
          res.json({"response": true});
        }
      });
    }
  });

  app.get('/student', function(req, res){
    Student.find(function(err, users){
      if (err) {
        res.send(err);
      }
      res.json(users);
    });
  });


  app.post('/facultyAccess', function(req, res){
    var id = req.body.id;
    var pass = req.body.password;
    for(var i = 0; i < Example.faculty.length; i++){
      if (id == Example.faculty[i].id && pass == Example.faculty[i].password){
        console.log("Success");
        console.log(Example.faculty[i]);
        res.send(Example.faculty[i]);
      }
    }
    res.json({"error": true, "message": "User does not exist!!!" });
  });

  app.post('/studentAccess', function(req, res){
    var rollno = req.body.rollno;
    for(var i = 0; i < Demo.students.length; i++){
      if (rollno == Demo.students[i].rollno) {
        console.log('Success');
        console.log(Demo.students[i]);
        res.send(Demo.students[i]);

        var message = new gcm.Message({
          notification: {
            title: "Marks and Attendance Record System",
            body: "Welcome to MARS!!"
          }
        });

        sender.send(message, {registrationTokens: regTokens}, function(err, res){
          if (err) {
            console.log(err);
          }
          else {
            console.log(res);
          }
        });
      }
    }
    res.json({"error": true, "message": "Student's data does not exist!!!" });
  });

  app.post('/students', function(req, res){
    var list = req.body.classlist;
    if (list == "8-A"){
      console.log("Success");
      res.send(Demo);
    }
    res.json({"error": true, "message": "Class Not found!!!"});
  });

  app.post('/updateAttendance', function(req, res){
    var students = req.body.students;
    console.log(req.body.students);
    addStudent.update_attendance(students, function(found){
      console.log(found);
      res.json(found);
    });
  });

  app.post('/updateMarks', function(req, res){
    var students = req.body.students;
    addStudent.update_marks(students, function(found){
      console.log(found);
      res.json(found);
    });
  });

  app.post('/getAttendance', function(req, res){
    var rollno = req.body.rollno;
    Student.findOne({rollNumber: rollno}, function(err, student){
      if (err) {
        console.log(err);
        res.json({'response': 'fail'});
      } else {
        console.log('Success in fetching attendance of', student.name);
        res.json({"attendance": student.attendance});
      }
    });
  });

  app.post('/getMarks', function(req, res){
    var rollno = req.body.rollno;
    Student.findOne({rollNumber: rollno}, function(err, student){
      if (err) {
        console.log(err);
        res.json({'response': 'fail'});
      } else {
        console.log('Success in fetching marks of', student.name);
        res.json({"marks": student.marks});
      }
    });
  });
};
